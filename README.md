# GPX Metadata Editor
This program allows to easily modify metadata from the a GPX file. In its current version it allows to change:
*  Name
*  Author
*  Link


![alt text](img/GUI-V1.png)


# Notes

*  Currently only tested on Ubuntu


 # Downloads
 
 [Click here to go to the download page](https://gitlab.com/ricardmol2/gpx-metadata-editor/-/jobs/)