package ricard.molins.gpx_editor.files;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import ricard.molins.gpx_editor.Files.TrackPoint;

class TrackPointTests {

	@Test
	void trackPointGetDate() {
		System.out.println("trackPointGetDate ===========================");
		TrackPoint tp = new TrackPoint(41.780958, 2.397027, 829.344, "2015-11-22T07:01:41Z");
		String originalString = "<trkpt lat=\"41.780958\" lon=\"2.397027\"><ele>829.344</ele><time>2015-11-22T07:01:41Z</time></trkpt>";

		assertTrue(originalString.equalsIgnoreCase(tp.getTRKPTString()));
	}

}