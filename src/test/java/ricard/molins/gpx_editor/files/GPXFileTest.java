package ricard.molins.gpx_editor.files;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import ricard.molins.gpx_editor.Files.GPXFile;

public class GPXFileTest {

	GPXFile gpx;

	@BeforeEach
	void init() {

		String demoFIle = "src/test/java/matagalls-pel-collell-i-sant-marcal-des-de-sant-bernat-massi.gpx";
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		gpx = new GPXFile();

		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(demoFIle);
			gpx.parseMetadata(doc);
			gpx.parseTrack(doc);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Test
	void metadataParserTest() {
		System.out.println("metadataParserTest ===========================");

		String name = "Wikiloc - Matagalls pel Collell i Sant Marçal des de Sant Bernat (Massís de Montseny)";
		String author = "Germinal V.Germinal V. on Wikiloc";
		String link = "https://www.wikiloc.com/wikiloc/user.do?id=464080";

		assertTrue(gpx.getAuthor().equalsIgnoreCase(author));
		assertTrue(gpx.getName().equalsIgnoreCase(name));
		assertTrue(gpx.getLink().equalsIgnoreCase(link));

	}

	@Test
	void parseTrackTest() {

		System.out.println("parseTrackTest ==============================");
		String trackName = "Matagalls pel Collell i Sant Marçal des de Sant Bernat (Mass...";
		String trackComment = "Comment String";
		String trackDescription = "Description  lore ipsum";
		String fistPoint = "<trkpt lat=\"41.780958\" lon=\"2.397027\"><ele>829.344</ele><time>2015-11-22T07:01:41Z</time></trkpt>";
		String secondPoint = "<trkpt lat=\"41.780959\" lon=\"2.397025\"><ele>829.305</ele><time>2015-11-22T07:01:42Z</time></trkpt>";

		String gpx_trackName = gpx.getTrackName();
		String gpx_trackComment = gpx.getTrackComment();
		String gpx_trackDescription = gpx.getTrackDescription();
		String gpx_firstPoint = gpx.getTrackPoints().get(0).getTRKPTString();
		String gpx_secondPoint = gpx.getTrackPoints().get(1).getTRKPTString();

		assertTrue(gpx_trackName.equalsIgnoreCase(trackName));
		assertTrue(gpx_trackComment.equalsIgnoreCase(trackComment));
		assertTrue(gpx_trackDescription.equalsIgnoreCase(trackDescription));
		assertTrue(gpx_firstPoint.equalsIgnoreCase(fistPoint));
		assertTrue(gpx_secondPoint.equalsIgnoreCase(secondPoint));
	}

	@Test
	void getFileContentTest() {
		System.out.println("getFileContentTest ==============================");
		gpx.getFileContent();
		System.out.println("getFileContentTest ==============================");
		assertTrue(true);

	}
}
