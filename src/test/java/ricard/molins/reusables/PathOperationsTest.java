package ricard.molins.reusables;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class PathOperationsTest {
	
	@Test
	void filePathToFolderPath() {
		System.out.println("filePathToFolderPath ===========================");
		String filePath = "/home/path/more/file.gpx";
		String folderPath = "/home/path/more";
		System.out.println(filePath);
		System.out.println(PathOperations.FilePathToFolderPath(filePath));
	
		assertTrue(folderPath.equalsIgnoreCase(PathOperations.FilePathToFolderPath(filePath)));
	}

}
