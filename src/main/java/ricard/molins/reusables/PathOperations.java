package ricard.molins.reusables;

import java.io.File;
import java.io.IOException;

public class PathOperations {

	/**
	 * @param filePath Ex: /home/user/file.gpx
	 * @return path to the folder containing the file home/user
	 */
	public static String FilePathToFolderPath(String filePath) {

		File file;
		String userDir = filePath;
		String fodlerPath = null;
		file = new File(userDir);
		try {
			file = new File(userDir + "/..");
			fodlerPath = file.getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return fodlerPath;
	}

}
