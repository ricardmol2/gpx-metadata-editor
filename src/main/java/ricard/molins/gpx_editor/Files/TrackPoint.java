package ricard.molins.gpx_editor.Files;

public class TrackPoint {

	public double lat;
	public double lon;
	public double ele;
	public String date;

	public TrackPoint(double lat, double lon, double ele, String date) {
		this.lat = lat;
		this.lon = lon;
		this.ele = ele;
		this.date = date;
	}

	public String getTRKPTString() {
		String txt;

		txt = "<trkpt " + "lat=\"" + lat + "\" " + "lon=\"" + lon + "\"><ele>" + ele + "</ele>" + "<time>" + date
				+ "</time>" + "</trkpt>";

		return txt;
	}
}
