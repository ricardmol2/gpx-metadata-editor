package ricard.molins.gpx_editor.Files;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import ricard.molins.gpx_editor.GUI.MainGui;
import ricard.molins.reusables.PathOperations;

public class TrackEditor {

	private String filePath;
	private File gpxFilePath;
	private MainGui gui;
	private GPXFile gpx;
	private boolean guiActive;

	public TrackEditor(MainGui gui) {
		this.gui = gui;
		if (gui == null) {
			guiActive = false;
		} else {
			guiActive = true;
		}
	}

	public void setNewPath(String filePString) {
		if (filePString.endsWith(".gpx")) {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;
			System.out.println("New file loaded " + filePString);
			this.filePath = filePString;
			this.gpx = new GPXFile();
			gpxFilePath = new File(filePath);
			getLabels();
			try {
				dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(gpxFilePath);
				gpx.parseTrack(doc);
			} catch (ParserConfigurationException | SAXException | IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Not a GPX");
		}
	}

	private void getLabels() {
		String name, author, link;
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(gpxFilePath);
			gpx.parseMetadata(doc);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}

		name = gpx.getName();
		author = gpx.getAuthor();
		link = gpx.getLink();

		if (guiActive) {
			gui.printGPXMetadataInfo(name, author, link);
		}
	}

	private void setNewLabels() {
		HashMap<String, String> newLabels = gui.getGPXMetadataInfo();
		gpx.setName(newLabels.get("Name"));
		gpx.setAuthor(newLabels.get("Author"));
		gpx.setLink(newLabels.get("Link"));
	}

	public void saveToFile() {
		/* Updates the GPX File handler */
		setNewLabels();

		/* Creates the File */
		PrintWriter writer;
		try {
			String formatedName = gpx.getName().replace(" ", "-");
			String fileName = PathOperations.FilePathToFolderPath(filePath) + "/" + formatedName + ".gpx";
			writer = new PrintWriter(fileName, "UTF-8");
			System.out.println("New file saved" + fileName);
			writer.println(gpx.getFileContent());
			writer.flush();
			writer.close();

		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

	/*
	 * public static void main(String[] args) { TrackEditor editor = new
	 * TrackEditor(null); String demoFIle =
	 * "/home/ricard/eclipse-workspace/gpx_editor/src/main/java/ricard/molins/gpx_editor/matagalls-pel-collell-i-sant-marcal-des-de-sant-bernat-massi.gpx";
	 * try { editor.setNewPath(demoFIle); } catch (Exception e) {
	 * e.printStackTrace(); }
	 * 
	 * }
	 */

}
