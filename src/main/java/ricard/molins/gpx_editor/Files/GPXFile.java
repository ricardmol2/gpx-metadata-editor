package ricard.molins.gpx_editor.Files;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class GPXFile {

	/* GPX Metadata */
	private String name;
	private String author;
	private String link;

	/* GPX Track */
	private String trackName;
	private String trackComment;
	private String trackDescription;
	private ArrayList<TrackPoint> trackPoints;

	public GPXFile() {
	
	}

	public void parseMetadata(Document doc) {

		doc.getDocumentElement().normalize();
		//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		NodeList nList = doc.getElementsByTagName("metadata");
		//System.out.println("----------------------------");

		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);
			//System.out.println("\nCurrent Element :" + nNode.getNodeName());

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				name = eElement.getElementsByTagName("name").item(0).getTextContent();
				author = eElement.getElementsByTagName("author").item(0).getTextContent();
				//link = eElement.getElementsByTagName("link").item(0).getAttributes;
				Element e = (Element) eElement.getElementsByTagName("link").item(0);
				link = e.getAttribute("href");
				//System.out.println("Name : " + name);
				//System.out.println("Author " + author);

			}
		}

	}

	public void parseTrack(Document doc) {

		/* Declaring variables  */
		doc.getDocumentElement().normalize();
		//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		NodeList nList = doc.getElementsByTagName("trk");
		trackPoints = new ArrayList<TrackPoint>();
		double ele, lat, lon;
		String time;
		
		/* Parsing track name and other info */
		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				trackName = eElement.getElementsByTagName("name").item(0).getTextContent();
				trackComment = eElement.getElementsByTagName("cmt").item(0).getTextContent();
				trackDescription = eElement.getElementsByTagName("desc").item(0).getTextContent();
				//System.out.println("Trackname; " + trackName);
				//System.out.println("Track comment; " + trackComment);
			}
		}

		/* Parsing track points and storing them in the trackPoints array list */
		nList = doc.getElementsByTagName("trkpt");
		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				ele = Double.parseDouble(eElement.getElementsByTagName("ele").item(0).getTextContent());
				time = eElement.getElementsByTagName("time").item(0).getTextContent();
				lat = Double.parseDouble(eElement.getAttribute("lat"));
				lon = Double.parseDouble(eElement.getAttribute("lon"));
				//System.out.print(".");
				trackPoints.add(new TrackPoint(lat, lon, ele, time));

			}
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getTrackName() {
		return trackName;
	}

	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}

	public String getTrackComment() {
		return trackComment;
	}

	public void setTrackComment(String trackComment) {
		this.trackComment = trackComment;
	}

	public String getTrackDescription() {
		return trackDescription;
	}

	public void setTrackDescription(String trackDescription) {
		this.trackDescription = trackDescription;
	}
	
	public ArrayList<TrackPoint> getTrackPoints() {
		return trackPoints;
	}

	public String getFileContent() {
		String fullFile;
		
		fullFile = getHeader()+ "\n";
		fullFile += getMetadata()+ "\n";
		fullFile += getTrackBody()+ "\n";
		return fullFile;
	}
	
	
	private String getHeader() {
		String txt = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><gpx creator=\"Wikiloc - https://www.wikiloc.com\" version=\"1.1\" xmlns=\"http://www.topografix.com/GPX/1/1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\">";
		return txt;
		
	}
	
	private String getMetadata() {
		String txtM= "";
		txtM = "<metadata>"
				+ "<name>" + name + "</name>"
				+ "<author><name>" + author  + " </name></author>"
				+ "<link href=\"" + link + "\"></link>"
				+ "</metadata>";
		return txtM;
	}
	
	private String getTrackBody() {
		String txt = "";
		txt = "<trk>"
				+ "<name>"+ trackName +"</name> \n" 
				+ "<cmt>"+ trackComment+"</cmt> \n"
				+ "<desc>" + trackDescription + "</desc> \n"
				+"<trkseg>\n";	
		txt+=	getTrackPointSring();	
		txt += "</trkseg>\n" + 
				"</trk>\n" + 
				"</gpx>" ;
		return txt;
		
	}
	
	private String getTrackPointSring() {
		String txtP = "";
		
		for(int i = 0 ; i< trackPoints.size() ; i ++) {
			txtP += trackPoints.get(i).getTRKPTString()+ "\n";	
		}	
		return txtP;
		
	}
	

}
