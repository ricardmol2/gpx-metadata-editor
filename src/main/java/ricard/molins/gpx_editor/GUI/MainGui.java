package ricard.molins.gpx_editor.GUI;

import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;

import ricard.molins.gpx_editor.Files.TrackEditor;

public class MainGui {

	private JFrame frame;
	private FileTree fileTree;
	private Container contentPane;
	private JPanel p;
	private TrackEditor editor;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGui window = new MainGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		editor = new TrackEditor(this);
		frame = new JFrame();
		frame.setBounds(100, 100, 1133, 582);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		// TODO Get path of current directory
		fileTree = new FileTree( new File("."));
		fileTree.setEditor(editor);
		fileTree.setBounds(12, 0, 586, 448);
		frame.getContentPane().add(fileTree);

		String[] labels = { "Name: ", "Author: ", "Link: " };
		int numPairs = labels.length;

		// Create and populate the panel.
		p = new JPanel(new SpringLayout());
		p.setBounds(647, 0, 424, 455);
		for (int i = 0; i < numPairs; i++) {
			JLabel l = new JLabel(labels[i], JLabel.TRAILING);
			p.add(l);
			JTextArea textField = new JTextArea();
			l.setLabelFor(textField);
			textField.setLineWrap(true);
			p.add(textField);
		}

		// Lay out the panel.
		SpringUtilities.makeCompactGrid(p, numPairs, 2, // rows, cols
				6, 6, // initX, initY
				6, 6); // xPad, yPad

		frame.getContentPane().add(p);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editor.saveToFile();
			}
		});
		btnSave.setBounds(454, 485, 268, 40);
		frame.getContentPane().add(btnSave);

	}

	public TrackEditor getTrackEditor() {
		return this.editor;
	}

	public void printGPXMetadataInfo(String Name, String Author, String Link) {
		String s_array[] = { Name, Author, Link };
		int counter = 0;
		Component componentsp[] = p.getComponents();
		for (int i = 0; i < componentsp.length; i++) {
			if (componentsp[i] instanceof JTextArea) {
				((JTextArea) componentsp[i]).setText(s_array[counter]);
				counter++;
			}
		}

	}
	
	public HashMap<String, String> getGPXMetadataInfo() {
		HashMap<String, String> newData = new HashMap<String, String>();
		String s_array[] = { "Name", "Author", "Link" };
		Component componentsp[] = p.getComponents();
		int counter = 0; 
		for (int i = 0; i < componentsp.length; i++) {
			if (componentsp[i] instanceof JTextArea) {
				String fieldText = 	((JTextArea) componentsp[i]).getText();
				newData.put(s_array[counter], fieldText);
				counter++;
			}
		}
		return newData;
	}
}
